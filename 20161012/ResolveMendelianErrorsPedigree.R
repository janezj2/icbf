#!/usr/bin/env Rscript

library(sqldf)
library(gdata)
library(data.table)

## Prepare pedigree
Pedig <- read.table("PedigreeConflictf90.txt", header=FALSE, stringsAsFactors=FALSE)
#system("rm -f PedigreeConflictf90.txt")

Pedig$V5 <- c("20001231")
Pedig$V6 <- c("0.0")
Pedig$V7 <- c("0")

Pedig        <- Pedig[,c("V4","V1","V2","V3","V5","V6","V7")]
names(Pedig) <- c("Sex","Id","SireId","DamId","DOB","Val1","Val2")

## Check if there is any sire or dam that is in not in individual table
SireMissing <- Pedig[!(Pedig$SireId %in% Pedig$Id) & Pedig$SireId!=0, ]
DamMissing  <- Pedig[!(Pedig$DamId %in% Pedig$Id) & Pedig$DamId!=0, ]

if (nrow(SireMissing) == 0) {
  print("All sires are listed as individuals as well")
} else {
  SireMissing
}

if (nrow(DamMissing) == 0) {
  print("All dams are listed as individuals as well")
} else {
  DamMissing
}

## Recode pedigree so that it will fit to conflict
Pedig$IdSeq   <- 1:nrow(Pedig)

Sires <- Pedig[Pedig$Id %in% Pedig$SireId,]
Dams  <- Pedig[Pedig$Id %in% Pedig$DamId,]

Sires <- sqldf("select b.IdSeq SireSeq from Pedig a left join Sires b on a.SireId = b.Id")
Dams  <- sqldf("select b.IdSeq DamSeq from Pedig a left join Dams b on a.DamId = b.Id")

Pedig <- cbind(Pedig,Sires,Dams)
Pedig[is.na(Pedig)] <- 0

write.fwf(Pedig[,c("Sex","IdSeq","SireSeq","DamSeq","DOB","Val1","Val2")], file="PedigreeConflictf90.txt", width=c(3,9,8,8,8,4,4), colnames=FALSE, rownames=FALSE, quote=FALSE)
write.fwf(Pedig[,c("Id","SireId","DamId","Sex","IdSeq","SireSeq","DamSeq","DOB","Val1","Val2")], file="Pedig.txt", width=c(11,11,11,3,9,8,8,8,4,4), colnames=TRUE, rownames=FALSE, quote=FALSE)

#print("Pedigree preparation finished")
