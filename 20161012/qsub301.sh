#!/bin/sh
########################################
#                                      #
# GE job script for ECDF Cluster       #
#                                      #
########################################

#
# Grid Engine options
#$ -cwd
#$ -pe sharedmem 4
#$ -l h_vmem=16G
#$ -l h_rt=200:00:00
#$ -P roslin_hickey_group

# Standard report


echo "Working directory:"
pwd
date

#echo
#echo "System PATH (default):"
#echo $PATH
#echo "System PATH modified:"
export PATH=.:~/bin:$PATH
#echo $PATH
#echo "Ulimit:"
#ulimit -a
#echo

echo "Starting job:"
./AlphaImputev1.5.5-47-g5201af7 > AlphaImpute.log #2>&1 &

# CPU and RAM tracking                                                                               
#JOB=$!
#./cpumemlog.sh $JOB #-t=.1
#cpumemlogplot.R cpumemlog.${JOB}.txt

# Standard report                                                                                    
echo
pwd
date
