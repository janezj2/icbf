#!/bin/sh

#$ -N PrepGenoXXBREEDXX
#$ -cwd
#$ -o PrepGeno.out
#$ -e PrepGeno.err
#$ -l h_rt=2:00:00
#$ -R y
#$ -pe sharedmem 2
#$ -l h_vmem=8G

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load roslin/plink/3.36-beta
module load igmm/apps/R/3.3.0

# Standard report

echo "Working directory:"
pwd
date

export OMP_NUM_THREADS=8

export G=~/Hpc/Illumina/Janez/Genotypes
export A=~/Hpc/Illumina/Janez/Analysis
export I=~/Hpc/Illumina/Janez/Analysis/SelectedInd
export S=~/Hpc/Illumina/Janez/Scripts
export ind=~/Hpc/Illumina/Janez/Analysis/SelectedInd

## Check the number of common SNP that are only on Hd chip and not on idbv3
sort $G/idbv3.map -u > SortSnpsOnIdbv3.txt
sort $G/hd.map -u > SortSnpsOnHd.txt

comm -23 SortSnpsOnIdbv3.txt SortSnpsOnHd.txt > SNPsOnIdbv3AndNotHd.txt
rm SortSnpsOnIdbv3.txt SortSnpsOnHd.txt

## Extract only SNP names for the purpose of extraction using plink
cut -d " " -f 2 $G/idbv3.map > SnpsOnIdbv3.txt
cut -d " " -f 2 $G/hd.map > SnpsOnHd.txt

## Extract pure breed individuals and SNP from idbv3 chip only
plink --file $G/hd --keep $I/XXBREEDXX.txt --extract SnpsOnIdbv3.txt --cow --recode --out LmHd
plink --file $G/idbv3 --keep $I/XXBREEDXX.txt --extract SnpsOnIdbv3.txt --cow --recode --out LmIdbv3

## Merge idbv3 and hd genotypes and use only SNP that are on both SNP chips (using SNP that are only on hd SNP chip is enough as we already before extracted the those that are on idbv3 SNP chip only)
plink --file LmHd --merge LmIdbv3.ped LmIdbv3.map --cow --extract SnpsOnHd.txt --missing --het --recode --out LmHdIdbv3

## Extrac the individuals that have genotype call less than 0.9
tail -n +2 LmHdIdbv3.imiss | awk '$6 >= 0.10 { print $1,$2 }' > IDsWithCallRateLessThan0.90LmHdIdbv3.txt

## Draw plot for percentage of genotype call rate and heterozygosity rate for individuals
$S/DrawHetMissPlotInd.R LmHdIdbv3

## Remove individuals with low genotype call rate and heterozygosity rate out of range +-6SD of heterozygosity
plink --file LmHdIdbv3 --remove IndividualsToExlcudeMissHet.txt --missing --recode --cow --out LmHdIdbv3CleanInds

## Extract SNP with missing information on more than 10% of genotypes
tail -n +2 LmHdIdbv3CleanInds.lmiss | awk '$5 > 0.10 { print $1,$2 }' > MarkersWithCallRateLessThan0.90Plink.txt

## Draw plot for percentage of genotype call rate and heterozygosity rate for markers
$S/DrawMissPlotMarkers.R LmHdIdbv3CleanInds

## Remove SNP with missing information on more than 10% of genotypes, SNP with minor allele frequency less than 0.05 and SNP that are not in HW equilibirum
plink --file LmHdIdbv3CleanInds --recode --geno 0.10 --hwe 0.000001 --maf 0.05 --cow --out LmHdIdbv3CleanIndsMarkers


# Standard report
echo
pwd
date
