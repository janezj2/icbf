#!/bin/bash

## Create working directory for AlphaSeqOpt

export prog=~/Hpc/Illumina/Janez/Programs
export work=~/Hpc/Illumina/Janez
export ind=~/Hpc/Illumina/Janez/Analysis/SelectedInd
export nCHR="29" #Total number of chromosomes

## Load module (this is required for AlphaSeqOpt)
module load intel/2016

cd ${work}/Analysis

if [[ ! -d AlphaSeqOpt ]]; then
  mkdir AlphaSeqOpt
fi

cd AlphaSeqOpt

for i in ${ind}/*; do

  a=`echo $i | sed -e 's/.*\///' -e 's/.txt//'`

  if [ ! -d $a ]; then
    mkdir $a
  fi

  cd $a

  if [[ ! -d Phase ]]; then
    mkdir Phase
  fi

  ln -sf ${work}/Pedigree/${a}/AlphaRecodedPedigree.txt Pedigree.txt
  ln -sf ${work}/PreSequenceInfo/IndAlreadySequenced.txt .
  ln -sf ${prog}/AlphaSuite/AlphaSeqOpt .
  cp -rf ${prog}/AlphaSuite/AlphaSeqOptSpec.txt .

  b=`wc -l Pedigree.txt | cut -d" " -f1`

  sed -i "s/XXNROFINDXX/${b}/" AlphaSeqOptSpec.txt

  cd Phase

  for j in `seq 1 1 ${nCHR}`; do

    ln -sf ${work}/Analysis/Chromosomes/${a}/Chromosome${j}/Phasing/PhasingResults/FinalPhase.txt Chromosome${j}.txt

    c=`head -n 1 ${work}/Analysis/Chromosomes/${a}/Chromosome${j}/ch_coded | cut -d " " -f 2- | awk '{print NF}'`

    sed -i "s/XXNROFSNPCHR${j}XX/${c}/" ../AlphaSeqOptSpec.txt

  done

  cd ..

  cp -rf ${prog}/AlphaSuite/qsub_AlphaSeqOpt.sh .
  sed -i "s/XXBREEDXX/${a}/g" qsub_AlphaSeqOpt.sh

  rm -f ASeqOpt.err ASeqOpt.out

  qsub -hold_jid AP${a}Chr1,AP${a}Chr2,AP${a}Chr3,AP${a}Chr4,AP${a}Chr5,AP${a}Chr6,AP${a}Chr7,AP${a}Chr8,AP${a}Chr9,AP${a}Chr10,AP${a}Chr11,,AP${a}Chr12,AP${a}Chr13,AP${a}Chr14,AP${a}Chr15,AP${a}Chr16,AP${a}Chr17,AP${a}Chr18,AP${a}Chr19,AP${a}Chr20,AP${a}Chr21,AP${a}Chr22,AP${a}Chr23,AP${a}Chr24,AP${a}Chr25,AP${a}Chr26,AP${a}Chr27,AP${a}Chr28,AP${a}Chr29 qsub_AlphaSeqOpt.sh

  cd ..

done
