#!/usr/bin/env Rscript

library(sqldf)
library(gdata)
library(data.table)

#print("Started conflict.f90")

WD <- getwd()

## Prepare ChromosomeDataConflictf90.txt file for conflictf90
MarkerLocation         <- read.table("Markers.map", header=FALSE, stringsAsFactors=FALSE)
MarkerLocation$Chip1   <- 1:nrow(MarkerLocation)
MarkerLocation$Within  <- 1:nrow(MarkerLocation)
MarkerLocation$Overall <- 1:nrow(MarkerLocation)
MarkerLocation$Chips   <- 1
MarkerLocation <- MarkerLocation[,c(2,1,6,7,4,8,5)]

names(MarkerLocation) <- c("SnpName","Chrome","Within","Overall","Location","Nchips","Chip1")

MarkerLocation$SnpNameR <- rep("Marker000000000", nrow(MarkerLocation))

for (k in 1:nrow(MarkerLocation)) {
  MarkerLocation[k,"SnpNameR"] <- paste(substr(MarkerLocation[k,"SnpNameR"], 1, (15-nchar(MarkerLocation[k,"Chip1"]))),MarkerLocation[k,"Chip1"],sep="")
}

write.fwf(file="ChromosomeDataConflictf90.txt", MarkerLocation[,c(8,2,3,4,5,6,7)], colnames=FALSE, rownames=FALSE, quote=FALSE, width=c(15,5,9,9,12,5,9), sep="")
system("sed -i -e '1iSNPname chrome within overall location n_chips chip1\' ChromosomeDataConflictf90.txt")

## Prepare GenotypesRawConflictf90.txt file where genotypes will be stored
#  print("Prepare GenotypesRawConflictf90.txt file")
system("tail -n +2 chr.raw | cut -d \" \" -f 7- | sed \"s/ //g\" | sed \"s/NA/5/g\" > Geno.txt")
system("NumMarkers=($(wc -l Markers.map | awk '{ print $1 }'))
        cut -d \" \" -f 2 chr.raw | tail -n +2 | sed -e \"s/$/\t1/g\" -e \"s/$/\t$NumMarkers/g\" | paste - Geno.txt > GenotypesConflictf90.txt")

## Recreate the first 3 columns with recoded IDs
system("awk '{ print $1,$2,$3}' GenotypesConflictf90.txt > FirstThreeColumnsForConflict.txt")
system("awk '{ print $4}' GenotypesConflictf90.txt > GenotypeForConflict.txt")
ConfID <- read.table("FirstThreeColumnsForConflict.txt", header=FALSE, stringsAsFactors=FALSE)
names(ConfID) <- c("Id","Chip","SnpNum")
Pedig <- read.table("../Pedig.txt", header=TRUE, stringsAsFactors=FALSE)
ConfID <- sqldf("select a.*, b.IdSeq from ConfID a left join Pedig b on a.Id=b.Id")
write.fwf(file="FirstThreeColumnsForConflict.txt", ConfID[,c(4,2,3)], width=c(10,10,10), sep="", colnames=FALSE, rownames=FALSE)
system("paste -d \" \" FirstThreeColumnsForConflict.txt GenotypeForConflict.txt > GenotypesConflictf90.txt")
system("OMP_NUM_THREADS=5
        ./conflict > Conflictf90.log
        ")
system("awk '{ print $4 }' genotypes.txt | sed 's/./& /g' | sed 's/5/9/g' | sed 's/[[:blank:]]*$//'  > Genotypes.raw")
system("tail -n +2 chr.raw | awk '{ print $2 }' | paste -d \" \" - Genotypes.raw > ch_coded")
system("rm -f Geno.txt GenotypeForConflict.txt FirstThreeColumnsForConflict.txt Genotypes.raw")
