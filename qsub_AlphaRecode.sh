#!/bin/sh

#$ -N AlpRecXXBREEDXX
#$ -cwd
#$ -o AlphaRecode.out
#$ -e AlphaRecode.err
#$ -l h_rt=24:00:00
#$ -R y
#$ -pe sharedmem 1
#$ -l h_vmem=4G

# Standard report

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load igmm/apps/R/3.3.0

echo "Working directory:"
pwd
date

export OMP_NUM_THREADS=8

./AlphaRecode > AlphaRecode.log  2>&1 &

# CPU and RAM tracking
JOB=$!
./cpumemlog.sh $JOB #-t=.1
#./cpumemlogplot.R cpumemlog_${JOB}.txt

# Standard report
echo
pwd
date
