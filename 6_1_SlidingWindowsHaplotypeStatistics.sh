#!/bin/bash

########################################
#                                      #
# Haplotypes summary and discovery     #
# of lethal haplotypes                 #
#                                      #
########################################

## Change these locations appropriately
export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts
export prog=~/Hpc/Illumina/Janez/Programs
export CoreLength="20"
export SlidingWindow=`seq 1 1 20`

## cd ${pedig} || exit
cd ${A} || exit

## Now work on HaplotypeSummary

if [[ ! -d HaplotypeSummary ]]; then
  mkdir HaplotypeSummary
fi

cd HaplotypeSummary || exit

for i in ${A}/SelectedInd/*; do
  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
  if [ ! -d ${a} ]; then
    mkdir ${a}
  fi
  cd ${a} || exit
  for k in ${CoreLength}; do
    if [[ ! -d CoreLength${k} ]]; then
      mkdir CoreLength${k}
    fi
    cd CoreLength${k} || exit
    for l in ${SlidingWindow}; do
      if [[ ! -d SlidingWindow${l} ]]; then
        mkdir SlidingWindow${l}
      fi
      cd SlidingWindow${l} || exit
      cp -rf ${S}/qsub_SlidingWindowsHaplotypeStatistics.sh .
      sed -i -e "s/XXBREEDXX/${a}/g" -e "s/XXCORELENGTHXX/${k}/g" -e "s/XXSLIDWINXX/${l}/g" qsub_SlidingWindowsHaplotypeStatistics.sh
      ln -sf ${S}/SlidingWindowsHaplotypeStatistics.R
      ln -sf ${prog}/cpumemlog/cpumemlog.sh
      ln -sf ${prog}/cpumemlog/cpumemlogplot.R
      qsub -hold_jid R03APASO${a}*SlidingWindow${l} qsub_SlidingWindowsHaplotypeStatistics.sh
      cd ..
    done
    cd ..
  done
  cd ..
done
