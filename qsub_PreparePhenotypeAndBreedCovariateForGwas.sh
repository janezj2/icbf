#!/bin/sh

# Grid Engine options
#$ -cwd
#$ -N PreparePhenotypeAndBreedCovariateForGwas
#$ -o PreparePhenotypeAndBreedCovariateForGwas.out
#$ -e PreparePhenotypeAndBreedCovariateForGwas.err
#$ -pe sharedmem 1
#$ -l h_vmem=32G
#$ -l h_rt=2:00:00
#$ -P roslin_hickey_group

# Path to the folder with scripts
export S=~/Hpc/Illumina/Janez/Scripts

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load modules
module load roslin/plink/3.36-beta
module load igmm/apps/R/3.3.0

# Standard report
echo "Working directory:"
pwd
date
echo "Starting job:"

cd ..

cd EPD_for_FaSTLMM

rm AllocateBreed.R

cat << EOF > AllocateBreed.R
#!/usr/bin/env Rscript

## Set up working directory and read in the data
#setwd("/Users/jjenko/Desktop/ICBF_genotypes_batch2")
#setwd("~Hpc/Illumina/Janez/EPD_for_FaSTLMM")

#install.packages("compare")
#library(compare)
library(dplyr)

BV    <- read.csv(file="1M_44_EPDs.csv", header=TRUE, stringsAsFactors = FALSE)
BV    <- distinct(BV)
Geno  <- read.table(file="../Gwas/FinalIdbv3.fam", header=FALSE, stringsAsFactors = FALSE)
Breed <- read.csv(file="../BreedComposition/Breed_composition.csv", header=TRUE, stringsAsFactors = FALSE)

## As there are some animals with letters in the IDs remove them as it is wrong and change ID data type into integer
is.letter    <- function(x) grepl("[[:alpha:]]", x)
Breed        <- Breed[!(is.letter(Breed\$sample)), ]
Breed\$sample <- as.integer(Breed\$sample)

## Create files for each EPD separately and write it ta a file
for (i in names(BV)) {
  if (i %in% c("CARC_WT","CARC_CONF","CARC_FAT","CULL_COW","WEAN_WT","LIVE_WT","FEED_IN","CALF_QU","MAT_WW","VERY_HVC","HVC","MVC","LVC","CALF_PR","COW_SURV","CIV","AFC","CD","GL","MORT","MCD","DOC")) {
    Result                         <- merge(Geno[,c("V1","V2")], BV[,c("TECHID",i)], by.x = "V2", by.y = "TECHID", all.x=TRUE)
    Result[!(is.na(Result[,3])),3] <- Result[!(is.na(Result[,3])),3] + abs(min(na.omit(Result[,3])))
    Result[is.na(Result[,3]),3]    <- -9
    write.table(Result[,c(2,1,3)], file=paste(i,".txt", sep=""), col.names=FALSE, row.names=FALSE, quote=FALSE)
  }
}

#setwd("/Users/jjenko/Desktop/ICBF_genotypes_batch2")

## Create breed groups so that: - All the purebred animals create their own group (each breed separately)
##                              - Crossbreds with more than 50% of one bred create their own group (each breed separately)
##                              - All the rest create one group only
Breed                                             <- Breed[Breed\$sample %in% Geno\$V2, ]
Breed\$Group                                       <- NA
Breed[Breed\$fract1==32, "Group"]                  <- paste("Pure", Breed[Breed\$fract1==32, "br1"], sep="")
Breed[Breed\$fract1>16 & Breed\$fract1<32, "Group"] <- paste("Cross", Breed[Breed\$fract1>16 & Breed\$fract1<32, "br1"], sep="")
Breed[is.na(Breed\$Group), "Group"]                <- "Crossbred"
Breed[Breed\$fract1==0, "Group"]                   <- "NotSpecified"

Codes                                          <- data.frame(sort(unique(Breed\$Group)))
Codes\$Seq                                      <- 1:nrow(Codes)
names(Codes)[1]                                <- c("BreedCodes")
Codes[Codes\$BreedCodes=="NotSpecified", "Seq"] <- -9
Codes\$BreedCodes                               <- as.character(Codes\$BreedCodes)

Breed            <- merge(Breed, Codes, by.x="Group", by.y="BreedCodes")

## Merge genotype IDs with coded breeds 
GenoBreed        <- merge(Geno, Breed[,c("sample","Seq")], by.x="V2", by.y="sample", all.x=TRUE)
GenoBreed        <- GenoBreed[,c("V1","V2","Seq")]

## As there are some doubled data in Breed file extract only unique lines
GenoBreed        <- unique(GenoBreed)

## When there is no information about the breed composition put in there -9
GenoBreed[is.na(GenoBreed\$Seq), "Seq"] <- -9

## Add letter a at the end of the string to make sure that FaST-LMM will take it as class variable
GenoBreed\$Seq <- paste0(GenoBreed\$Seq,"a")

## Write out the covariate file for correction of population structure in GWAS analysis
write.table(GenoBreed, file="../BreedComposition/CovariateBreedIdbv3Gwas.txt", quote=FALSE, col.names=FALSE, row.names=FALSE, sep='\t')
EOF

chmod +700 AllocateBreed.R

./AllocateBreed.R

rm AllocateBreed.R

# Standard report
echo
pwd
date
