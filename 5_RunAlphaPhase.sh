#!/bin/bash

########################################
#                                      #
# GE job script for ECDF Cluster       #
#                                      #
########################################

## Change these file locations appropriately
export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts
export prog=~/Hpc/Illumina/Janez/Programs
export fCHR="1" #From chromosomes
export tCHR="29" #To chromosomes

if [ -z $1 ]; then
echo "Choose where to start:
   1. Run AlphaPhase
   2. Run AlphaImpute
   3. Run AlphaPhase for AlphaSeqOpt
   4. Run AlphaPhase for discovery of lethal haplotypes for different core lenghts
   5. Run AlphaPhase for sliding windows
   "
       read PROG
else
 export PROG=$1
 echo $PROG
fi

if [ "$PROG" == "1" ]; then

  echo "Run AlphaPhase"
  echo
  echo

  cd ${A}/Chromosomes || exit

  for i in ${A}/SelectedInd/*; do
    a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
    cd ${a} || exit

    for j in `seq ${fCHR} 1 ${tCHR}` ; do

      while [ ! -d Chromosome${j} ]; do
        sleep 10m
      done

      cd Chromosome${j}/ || exit

      ln -sf ${prog}/AlphaSuite/AlphaImputev1.9.1-8-gb793678 AlphaImpute
      ln -sf ${prog}/cpumemlog/cpumemlog.sh .
      ln -sf ${prog}/cpumemlog/cpumemlogplot.R .

      cat ${S}/qsub_5_RunAlphaPhase_RO1.sh | sed -e "s/XXBREEDXX/${a}/" \
                                                 -e "s/XXCHROMXX/${j}/" > qsub_5_RunAlphaPhase_RO1.sh

      cat ${S}/qsub_5_RunAlphaPhase_RO2.sh | sed -e "s/XXBREEDXX/${a}/" \
                                                 -e "s/XXCHROMXX/${j}/" > qsub_5_RunAlphaPhase_RO2.sh

      ln -sf ${A}/Pedigree/${a}/AlphaRecodedPedigree.txt input.ped
      ln -sf ch_coded input.gen

      b=`head -n 1 ch_coded | cut -d " " -f 2- | awk '{print NF}'`

      cat ${prog}/AlphaSuite/AlphaImputeSpec1.9-13-gaad8e8c.txt | sed -e "s/XXNROFSNPXX/${b}/" \
                                                                      -e "s/XXNRPROCAVXX/20/" \
                                                                      -e "s/XXBYPASSXX/Yes/" > AlphaImputeSpecTmp.txt

      cat AlphaImputeSpecTmp.txt | sed -e "s/XXRESOPTXX/1/" > AlphaImputeSpecRO1.txt
      cat AlphaImputeSpecTmp.txt | sed -e "s/XXRESOPTXX/2/" > AlphaImputeSpecRO2.txt
      rm -f AlphaImputeSpecTmp.txt

      qsub -hold_jid PrepChr${a} qsub_5_RunAlphaPhase_RO1.sh
#      qsub -hold_jid RO1${a}ch${j} qsub_5_RunAlphaPhase_RO2.sh

      cd - || exit
    done
    cd ${A}/Chromosomes || exit
  done


elif [ "$PROG" == "2" ]; then

  echo "Run Alphaimpute"
  echo
  echo

  cd ${A}/Chromosomes || exit

  for i in ${A}/SelectedInd/*; do
    a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
    cd ${a} || exit

    for j in `seq ${fCHR} 1 ${tCHR}` ; do

      while [ ! -d Chromosome${j} ]; do
        sleep 10m
      done

      cd Chromosome${j}/ || exit

      qsub -hold_jid RO1${a}ch${j} qsub_5_RunAlphaPhase_RO2.sh

      cd - || exit
    done
    cd ${A}/Chromosomes || exit
  done


elif [ "$PROG" == "3" ]; then

  echo "Run AlphaPhase for AlphaSeqOpt"
  echo
  echo

  cd ${A}/Chromosomes || exit
  for i in ${A}/SelectedInd/*; do
    a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
    cd ${a} || exit
    for j in `seq ${fCHR} 1 ${tCHR}` ; do

      while [ ! -d Chromosome${j} ]; do
        sleep 10m
      done

      cd Chromosome${j} || exit

      if [ ! -d "Phasing" ]; then
        mkdir Phasing
      fi

      cd Phasing || exit
      ln -sf ${prog}/cpumemlog/cpumemlog.sh .
      ln -sf ${prog}/cpumemlog/cpumemlogplot.R .
      ln -sf ${prog}/AlphaSuite/alphaphase_AlphaSeqOpt alphaphase
      ln -sf ../Results/ImputeGenotypes.txt input.gen
      ##ln -sf ../Results/ImputePhaseHMM.txt input.gen
      ln -sf ${A}/Pedigree/${a}/AlphaRecodedPedigree.txt input.ped

      if [ -e ../ch_coded ]; then
        b=`head -n 1 ../ch_coded | cut -d " " -f 2- | awk '{print NF}'`
      else
        b=`head -n 1 ../Results/ImputeGenotypesHMM.txt | cut -d " " -f 2- | awk '{print NF}'`
      fi

      cat ${prog}/AlphaSuite/AlphaPhaseSpec_AlphaSeqOpt.txt | sed -e "s/XXNROFSNPXX/${b}/" \
                                                                  -e "s/XXCORETAILLENGTHXX/400/" \
                                                                  -e "s/XXCORELENGTHXX/100/" > AlphaPhaseSpec.txt
      cat ${S}/qsub_5_RunAlphaPhaseForAlphaSeqOpt.sh | sed -e "s/XXBREEDXX/${a}/" \
                                                           -e "s/XXCHROMXX/${j}/" \
                                                           -e "s/XXCORELENGTHXX/100/" > qsub_5_RunAlphaPhaseForAlphaSeqOpt.sh

      rm -f APhase.err APhase.out

      qsub -hold_jid RO2${a}ch${j} qsub_5_RunAlphaPhaseForAlphaSeqOpt.sh
      sleep 1s
      cd ${A}/Chromosomes/${a} || exit
    done
    cd ${A}/Chromosomes || exit
  done


elif [ "$PROG" == "4" ]; then

  echo "Run AlphaPhase for discovery of lethal haplotypes for different core lenghts"
  echo
  echo

  cd ${A}/Chromosomes || exit
  for i in ${A}/SelectedInd/*; do
    a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
    cd ${a} || exit
    for j in `seq ${fCHR} 1 ${tCHR}` ; do

      while [ ! -d Chromosome${j} ]; do
        sleep 10m
      done

      cd Chromosome${j} || exit

      if [ ! -d "Phasing" ]; then
        mkdir Phasing
      fi

      cd Phasing || exit

      for k in 5 10 20 50 100 200 500 1000; do

        coretail=$((k + 300))        

        if [ ! -d "PhasingCoreLength${k}" ]; then
          mkdir PhasingCoreLength${k}
        fi

        cd PhasingCoreLength${k} || exit

        ln -sf ${prog}/cpumemlog/cpumemlog.sh .
        ln -sf ${prog}/cpumemlog/cpumemlogplot.R .
        ln -sf ${prog}/AlphaSuite/alphaphase_AlphaSeqOpt alphaphase
        ln -sf ../../Results/ImputeGenotypes.txt input.gen
        ##ln -sf ../Results/ImputePhaseHMM.txt input.gen
        ln -sf ${A}/Pedigree/${a}/AlphaRecodedPedigree.txt input.ped

        if [ -e ../../ch_coded ]; then
          b=`head -n 1 ../../ch_coded | cut -d " " -f 2- | awk '{print NF}'`
        else
          b=`head -n 1 ../../Results/ImputeGenotypesHMM.txt | cut -d " " -f 2- | awk '{print NF}'`
        fi

        cat ${prog}/AlphaSuite/AlphaPhaseSpec_AlphaSeqOpt.txt | sed -e "s/XXNROFSNPXX/${b}/" \
                                                                    -e "s/XXCORETAILLENGTHXX/${coretail}/" \
                                                                    -e "s/XXCORELENGTHXX/${k}/" > AlphaPhaseSpec.txt
        cat ${S}/qsub_5_RunAlphaPhaseForAlphaSeqOpt.sh | sed -e "s/XXBREEDXX/${a}/" \
                                                             -e "s/XXCHROMXX/${j}/" \
                                                             -e "s/XXCORELENGTHXX/${k}/" > qsub_5_RunAlphaPhaseForAlphaSeqOpt.sh

        rm -f APhase.err APhase.out

        qsub -hold_jid RO2${a}ch${j} qsub_5_RunAlphaPhaseForAlphaSeqOpt.sh
        sleep 1s
        cd ${A}/Chromosomes/${a}/Chromosome${j}/Phasing || exit
      done
      cd ${A}/Chromosomes/${a} || exit
    done
    cd ${A}/Chromosomes || exit
  done


elif [ "$PROG" == "5" ]; then

  echo "Run AlphaPhase for sliding windows"
  echo
  echo

  cd ${A}/Chromosomes || exit
  for i in ${A}/SelectedInd/*; do
    a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
    cd ${a} || exit
    for j in `seq ${fCHR} 1 ${tCHR}` ; do

      while [ ! -d Chromosome${j} ]; do
        sleep 10m
      done

      cd Chromosome${j} || exit

      if [ ! -d "Phasing" ]; then
        mkdir Phasing
      fi

      cd Phasing || exit

      for k in 20; do

        coretail=$((k + 300))

        if [ ! -d "PhasingCoreLength${k}" ]; then
          mkdir PhasingCoreLength${k}
        fi

        cd PhasingCoreLength${k} || exit

          for l in `seq 1 1 20`;do
          ## 20 is the length of ID, each genotype is of length 2 and 1 is subtracted because I need to keep space betwen ID and cutted genotypes
          RemoveSnp=$((l * 2 - 1 + 20))

          if [ ! -d "SlidingWindow${l}" ]; then
            mkdir SlidingWindow${l}
          fi

          cd SlidingWindow${l} || exit

          ln -sf ${prog}/cpumemlog/cpumemlog.sh .
          ln -sf ${prog}/cpumemlog/cpumemlogplot.R .
          ln -sf ${prog}/AlphaSuite/AlphaPhase-v1.3.6-27-g28b7941 alphaphase
          ##ln -sf ${prog}/AlphaSuite/alphaphase_AlphaSeqOpt alphaphase
          ln -sf ../../../Results/ImputeGenotypes.txt input.gen
          ##cut -c-20,${RemoveSnp}- ../../../Results/ImputeGenotypes.txt > input.gen
          ##ln -sf ../Results/ImputePhaseHMM.txt input.gen
          ln -sf ${A}/Pedigree/${a}/AlphaRecodedPedigree.txt input.ped

          if [ -e ../../../ch_coded ]; then
            nSnp=`head -n 1 ../../../ch_coded | cut -d " " -f 2- | awk '{print NF}'`
            nSnp=$((nSnp-l+1))
          else
            nSnp=`head -n 1 ../../../Results/ImputeGenotypesHMM.txt | cut -d " " -f 2- | awk '{print NF}'`
            nSnp=$((nSnp-l+1))
          fi

          ##cat ${prog}/AlphaSuite/AlphaPhaseSpec_AlphaSeqOpt.txt | sed -e "s/XXNROFSNPXX/${nSnp}/" \
          cat ${prog}/AlphaSuite/AlphaPhaseSpec-v1.3.6-27-g28b7941.txt | sed -e "s/XXNROFSNPXX/${nSnp}/" \
                                                                             -e "s/XXCORETAILLENGTHXX/${coretail}/" \
                                                                             -e "s/XXCORELENGTHXX/${k}/" > AlphaPhaseSpec.txt
          cat ${S}/qsub_5_RunAlphaPhaseForAlphaSeqOpt.sh | sed -e "s/XXBREEDXX/${a}/" \
                                                               -e "s/XXCHROMXX/${j}/" \
                                                               -e "s/XXCORELENGTHXX/${k}/" \
                                                               -e "s/XXSLIDINGWINDOWXX/${l}/" > qsub_5_RunAlphaPhaseForAlphaSeqOpt.sh

          ## This section creates CoreIndex.txt file where the information abot number of cores, number of SNPs, number of animals and the start and end for each of the SNP is stored
          nAni=`wc -l input.gen | awk '{print $1}'`
          nCor=$((${nSnp} / ${k}))
          
          echo ${nSnp} > CoreIndex.txt
          echo ${nAni} >> CoreIndex.txt
          
          for i in `seq ${k} ${k} ${nSnp}`; do
            echo $((${i} / ${k})) $((${i} - ${k} + 1)) ${i} >> CoreIndex.txt
          done
          
          if [ $((${nSnp} - ${nCor} * ${k})) -gt 0 ]; then
            nCor=$((${nCor} + 1))
            sed -i '1i\'${nCor} CoreIndex.txt
            From=`tail -1 CoreIndex.txt | awk '{print $3+1}'`
            echo ${nCor} ${From} ${nSnp} >> CoreIndex.txt
          else 
            sed -i '1i\'${nCor} CoreIndex.txt
          fi


          rm -f APhase.err APhase.out

          qsub -hold_jid RO2${a}ch${j} qsub_5_RunAlphaPhaseForAlphaSeqOpt.sh

          cd ${A}/Chromosomes/${a}/Chromosome${j}/Phasing/PhasingCoreLength${k} || exit
        done
        cd ${A}/Chromosomes/${a}/Chromosome${j}/Phasing || exit        
      done
      cd ${A}/Chromosomes/${a} || exit
    done
    cd ${A}/Chromosomes || exit
  done
fi

