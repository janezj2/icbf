#!/bin/sh
########################################
#                                      #
# GE job script for ECDF Cluster       #
#                                      #
########################################

#
# Grid Engine options
#$ -cwd
#$ -N RO2XXBREEDXXchXXCHROMXX
#$ -o RO2AImpute.out
#$ -e RO2AImpute.err
#$ -pe sharedmem 1
#$ -l h_vmem=32G
#$ -l h_rt=48:00:00
#$ -P roslin_hickey_group

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load igmm/apps/R/3.3.0

# Standard report


echo "Working directory:"
pwd
date

#echo
#echo "System PATH (default):"
#echo $PATH
#echo "System PATH modified:"
export PATH=.:~/bin:$PATH
#echo $PATH
#echo "Ulimit:"
#ulimit -a
#echo

ln -sf AlphaImputeRO2 AlphaImpute
ln -sf AlphaImputeSpecRO2.txt AlphaImputeSpec.txt

echo "Starting job:"
./AlphaImpute > AlphaImputeRO2.log 2>&1 &

# CPU and RAM tracking
JOB=$!
./cpumemlog.sh $JOB #-t=.1
#./cpumemlogplot.R cpumemlog_${JOB}.txt

# Standard report
echo
pwd
date
