#!/bin/sh

#$ -N ASeqOptCostsXXBREEDXXHapXXHAPLOTHRESXX
#$ -cwd
#$ -o ASeqOptCosts.out
#$ -e ASeqOptCosts.err
#$ -l h_rt=240:00:00
#$ -R y
#$ -pe sharedmem 1
#$ -l h_vmem=8G

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load intel/2016
module load igmm/apps/R/3.3.0

# Standard report

echo "Working directory:"
pwd
date

export OMP_NUM_THREADS=16

./AlphaSeqOpt > AlphaSeqOpt_Costs.log  2>&1 &

# CPU and RAM tracking
JOB=$!
./cpumemlog.sh $JOB #-t=.1
./cpumemlogplot.R cpumemlog_${JOB}.txt

# Standard report
echo
pwd
date
