
## Remove phasing files AFTER Imputatation is finished
## Run this from the folder of each set that are in the Chromosomes folder

rm PedigreeConflictf90.txt PrepChr.out Pedig.txt PrepChr.err qsub_4_PrepareChromosomes.sh
unlink AlphaRecodedPedigree.txt
unlink Pedigree.txt
for i in `seq 11 1 29`; do
  cd Chromosome${i} || exit
  unlink AlphaImputeSpec.txt
  unlink AlphaImputev1.6.2-16-ga63fe7e
  unlink AlphaImputev1.6.3
  unlink chromosome.data
  unlink conflict
  unlink genotypes.raw
  unlink input.gen
  unlink input.ped
  unlink pedigree.file
  rm Conflictf90.log conflict.options hetx marker.freq Markers.map P5AImpute.err P5AImpute.out qsub301.sh qsub303.sh qsub305.sh
  rm chr.raw ch_coded GenotypesConflictf90.txt genotypes.txt PedigreeConflictf90.txt Pedig.txt PrepChr.out clone.keys
  rm AlphaImpute.log AlphaImputeSpec2.txt AlphaImputeSpec3.txt APhase.err APhase.out ChromosomeDataConflictf90.txt
  rm MiscellaneousIndividualSnpInformativeness.txt MiscellaneousIndividualMendelianInformativeness.txt parent.errors marker.errors
  rm -r GeneProb InputFiles IterateGeneProb Miscellaneous
  cd Phasing || exit
  rm -r Phase*
  rm -r Miscellaneous
  rm AlphaPhase.log AlphaPhaseSpec.txt EditingSnpSummary.txt P8APhase.err P8APhase.out qsub306.sh qsub308.sh
  unlink input.gen
  unlink input.ped
  cd ../../ || exit
done


## Check if GeneProb has finished
rm -f GeneProbCheck.txt
for i in */ ; do
  cd ${i} || exit
  for j in `seq 1 1 29`; do
    cd Chromosome${j} || exit
    for k in `seq 1 1 20`; do
      if [ -e GeneProb/GeneProb${k}/GeneProbs.txt ] && [ -s GeneProb/GeneProb${k}/GeneProbs.txt ] ; then
        echo "${i}  Chromosome ${j} GeneProb/GeneProb${k}/GeneProbs.txt EXISTS" >> ../../GeneProbCheck.txt
      else
        echo "${i}  Chromosome ${j} GeneProb/GeneProb${k}/GeneProbs.txt MISSING" >> ../../GeneProbCheck.txt
      fi
    done
    cd ..
  done
  cd ..
done


## Check if PhasingI has finished
rm -f PhasingICheck.txt
for i in */ ; do
  cd ${i} || exit
  for j in `seq 1 1 29`; do
    cd Chromosome${j} || exit
    for k in `seq 1 1 20`; do
      if [ -e Phasing/Phase${k}/PhasingResults/FinalPhase.txt ] && [ -s Phasing/Phase${k}/PhasingResults/FinalPhase.txt ]; then
        echo "${i}  Chromosome ${j} Phasing/Phase${k}/PhasingResults/FinalPhase.txt EXISTS" >> ../../PhasingICheck.txt
      else
        echo "${i}  Chromosome ${j} Phasing/Phase${k}/PhasingResults/FinalPhase.txt MISSING" >> ../../PhasingICheck.txt
      fi
    done
    cd ..
  done
  cd ..
done


## Check if Imputation has finished
rm -f ImputationCheck.txt
for i in */ ; do
  cd ${i} || exit
  for j in `seq 1 1 29`; do
    cd Chromosome${j} || exit
    if [ -e Results/ImputeGenotypes.txt ] && [ -s Results/ImputeGenotypes.txt ]; then
      echo "${i}  Chromosome ${j} Results/ImputeGenotypes.txt EXISTS" >> ../../ImputationCheck.txt
    else
      echo "${i}  Chromosome ${j} Results/ImputeGenotypes.txt MISSING" >> ../../ImputationCheck.txt
    fi
    cd ..
  done
  cd ..
done


## Check if PhasingII has finished - This one is the original where I did not have different core lenghts
#rm -f PhasingIICheck.txt
#for i in */ ; do
#  cd ${i} || exit
#  for j in `seq 1 1 29`; do
#    cd Chromosome${j} || exit
#    if [ -e Phasing/PhasingResults/FinalPhase.txt ] && [ -s Phasing/PhasingResults/FinalPhase.txt ]; then
#      echo "${i}  Chromosome ${j} Phasing/PhasingResults/FinalPhase.txt EXISTS" >> ../../PhasingIICheck.txt
#    else
#      echo "${i}  Chromosome ${j} Phasing/PhasingResults/FinalPhase.txt MISSING" >> ../../PhasingIICheck.txt
#    fi
#    cd ..
#  done
#  cd ..
#done

## Check if PhasingII has finished - This one is for different core length
rm -f PhasingIICheck.txt
for i in */ ; do
  cd ${i} || exit
  for j in `seq 1 1 29`; do
    cd Chromosome${j} || exit
    for k in PhasingCoreLength5 PhasingCoreLength10 PhasingCoreLength50 PhasingCoreLength100 PhasingCoreLength200 PhasingCoreLength500 PhasingCoreLength1000; do
      if [ -e Phasing/${k}/PhasingResults/FinalHapIndCarry.txt ] && [ -s Phasing/${k}/PhasingResults/FinalHapIndCarry.txt ]; then
        echo "${i}  Chromosome ${j} Phasing/${k}/PhasingResults/FinalHapIndCarry.txt EXISTS" >> ../../PhasingIICheck.txt
      else
        echo "${i}  Chromosome ${j} Phasing/${k}/PhasingResults/FinalHapIndCarry.txt MISSING" >> ../../PhasingIICheck.txt
      fi
    done
    cd ..
  done
  cd ..
done


## Check if phasing for sliding windows of core length 20 is finished
rm -f PhasingCoreLength20SlidingWindowsCheck.txt
for i in */ ; do
  cd ${i} || exit
  for j in `seq 1 1 29`; do
    cd Chromosome${j} || exit
    for k in `seq 1 1 20`; do
      if [ -e Phasing/PhasingCoreLength20/SlidingWindow${k}/PhasingResults/FinalHapIndCarry.txt ] && [ -s Phasing/PhasingCoreLength20/SlidingWindow${k}/PhasingResults/FinalHapIndCarry.txt ]; then
        echo "${i}  Chromosome ${j} Phasing/PhasingCoreLength20/SlidingWindow${k}/PhasingResults/FinalHapIndCarry.txt EXISTS" >> ../../PhasingCoreLength20SlidingWindowsCheck.txt
      else
        echo "${i}  Chromosome ${j} Phasing/PhasingCoreLength20/SlidingWindow${k}/PhasingResults/FinalHapIndCarry.txt MISSING" >> ../../PhasingCoreLength20SlidingWindowsCheck.txt
      fi
    done
    cd ..
  done
  cd ..
done


## Script to rerun GeneProb
## Adapt chromosome number and GeneProb number appropriately
export S=~/Hpc/Illumina/Janez/Scripts
for j in `seq 3 1 3`; do
  cd Chromosome${j} || exit
  for k in `seq 19 1 20`; do
    cd GeneProb/GeneProb${k} || exit
    ln -s /home/jjenko/Hpc/Illumina/Janez/Programs/AlphaSuite/GeneProbForAlphaImpute .

    cat ${S}/qsub302.sh | sed -e "s/XXBREEDXX/Aa/" \
                                         -e "s/XXCHROMXX/${j}/" \
                                         -e "s/XXGENEPROBXX/${k}/" > qsub302.sh

    qsub qsub302.sh

    cd - || exit
  done
  cd ../ || exit
done



## Script to rerun PhasingII
## Adapt chromosome number appropriately
export a=Min50PercChInd
export A=~/Hpc/Illumina/Janez/Analysis_Min50Perc_LmChAaSiHe
export prog=~/Hpc/Illumina/Janez/Programs
export S=~/Hpc/Illumina/Janez/Scripts

for j in `seq 27 1 29` ; do
  cd Chromosome${j} || exit
  if [ ! -d "Phasing" ]; then
    mkdir Phasing
  fi
  cd Phasing || exit
  ln -sf ${prog}/AlphaSuite/alphaphase .
  ln -sf ../Results/ImputeGenotypesHMM.txt input.gen
  ln -sf ${A}/Pedigree/${a}/AlphaRecodedPedigree.txt input.ped

  if [ -e ../ch_coded ]; then
    b=`head -n 1 ../ch_coded | cut -d " " -f 2- | awk '{print NF}'`
  else
    b=`head -n 1 ../Results/ImputeGenotypesHMM.txt | cut -d " " -f 2- | awk '{print NF}'`
  fi

  cat ${prog}/AlphaSuite/AlphaPhaseSpec.txt | sed -e "s/XXNROFSNPXX/${b}/" > AlphaPhaseSpec.txt
  cat ${S}/qsub308.sh | sed -e "s/XXBREEDXX/${a}/" \
                                       -e "s/XXCHROMXX/${j}/" > qsub308.sh

  rm -f APhase.err APhase.out

  qsub -hold_jid P5${a}ch${j} qsub308.sh
  sleep 1s
  cd ${A}/Chromosomes/${a} || exit
done
cd ${A}/Chromosomes || exit


## Run AlphaSeqOpt from
export a=Min50PercAaInd
for i in 0 1 2 3 4 5 10; do
  cd ${i} || exit
  qsub -hold_jid P8AP${a}Chr1,P8AP${a}Chr2,P8AP${a}Chr3,P8AP${a}Chr4,P8AP${a}Chr5,P8AP${a}Chr6,P8AP${a}Chr7,P8AP${a}Chr8,P8AP${a}Chr9,P8AP${a}Chr10,P8AP${a}Chr11,P8AP${a}Chr12,P8AP${a}Chr13,P8AP${a}Chr14,P8AP${a}Chr15,P8AP${a}Chr16,P8AP${a}Chr17,P8AP${a}Chr18,P8AP${a}Chr19,P8AP${a}Chr20,P8AP${a}Chr21,P8AP${a}Chr22,P8AP${a}Chr23,P8AP${a}Chr24,P8AP${a}Chr25,P8AP${a}Chr26,P8AP${a}Chr27,P8AP${a}Chr28,P8AP${a}Chr29 qsub_AlphaSeqOpt.sh
  cd ..
donex

qdel 9700334 9700335 9700336 9700337 9700338 9700339 9700340
