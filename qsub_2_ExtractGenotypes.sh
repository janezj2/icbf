#!/bin/sh

#$ -N PrepGenoXXBREEDXX
#$ -cwd
#$ -o PrepGeno.out
#$ -e PrepGeno.err
#$ -l h_rt=48:00:00
#$ -R y
#$ -pe sharedmem 8
#$ -l h_vmem=8G

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load roslin/plink/3.36-beta
module load igmm/apps/R/3.3.0

# Standard report

echo "Working directory:"
pwd
date

export OMP_NUM_THREADS=8

export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export G=~/Hpc/Illumina/Janez/Genotypes
export S=~/Hpc/Illumina/Janez/Scripts
export geno=~/Hpc/Illumina/Janez/Genotypes

## ## Check the number of common SNP that are only on Hd chip and not on idbv3
## sort $G/idbv3.map -u > SortSnpsOnIdbv3.txt
## sort $G/hd.map -u > SortSnpsOnHd.txt

## comm -23 SortSnpsOnIdbv3.txt SortSnpsOnHd.txt > SNPsOnIdbv3AndNotHd.txt
## rm SortSnpsOnIdbv3.txt SortSnpsOnHd.txt

## Extract only SNP names for the purpose of extraction using plink
## cut -d " " -f 2 ${G}/idbv3.map > SnpsOnIdbv3.txt
## I have commented out the top one and use the bottom one as there were some SNPs with different positions in the delivey of SNPs from files idbv3-1 to idbv3-5 and idbv3-6
cp ${G}/../Gwas/SnpsOnIdbv3.txt .
## cut -d " " -f 2 $G/hd.map > SnpsOnHd.txt

## ## Extract pure breed individuals and SNP from idbv3 chip only
## plink --file $G/hd --keep $A/SelectedInd/XXBREEDXX.txt --extract SnpsOnIdbv3.txt --cow --recode --out LmHd
## plink --file $G/idbv3 --keep $A/SelectedInd/XXBREEDXX.txt --extract SnpsOnIdbv3.txt --cow --recode --out LmIdbv3

rm -f FilesToMerge.txt

for i in ${geno}/*.ped; do
  a=`echo ${i} | sed -e 's/.*\///' -e 's/.ped//'`
  echo ${a}
  ## Extract pure breed individuals and SNP from idbv3 chip only
  plink --file ${G}/${a} --keep ${A}/SelectedInd/XXBREEDXX.txt --extract SnpsOnIdbv3.txt --missing --het --cow --recode --out ${a}
  if [ -f ${a}.ped ]; then
    ## Extrac the individuals that have genotype call less than 0.9
    tail -n +2 ${a}.imiss | awk '$6 >= 0.10 { print $1,$2 }' > IDsWithCallRateLessThan0.90${a}.txt
    ## Draw plot for percentage of genotype call rate and heterozygosity rate for individuals
    ${S}/DrawHetMissPlotInd.R ${a}
    ## Remove individuals with low genotype call rate and heterozygosity rate out of range +-6SD of heterozygosity
    plink --file ${a} --remove IndividualsToExlcudeMissHet.txt --missing --recode --cow --out ${a}CleanInds
    mv IndividualsToExlcudeMissHet.txt IndividualsToExlcudeMissHet${a}.txt
    ## Extract SNP with missing information on more than 10% of genotypes
    tail -n +2 ${a}CleanInds.lmiss | awk '$5 > 0.10 { print $1,$2 }' > MarkersWithCallRateLessThan0.90Plink${a}.txt
    ## Draw plot for percentage of genotype call rate and heterozygosity rate for markers
    ${S}/DrawMissPlotMarkers.R ${a}CleanInds
    ## Remove SNP with missing information on more than 10% of genotypes
    plink --file ${a}CleanInds --recode --geno 0.10 --cow --out ${a}CleanIndsMarkers
    export b
    ## Add name of the file in the merge file with file names to merge
##    b=`wc -l Lm${a}CleanIndsMarkers.ped | cut -d " " -f 1`
    if [[ $(wc -l ${a}CleanIndsMarkers.ped | cut -d " " -f 1) -gt 0 ]]; then
      echo ${a}CleanIndsMarkers.ped ${a}CleanIndsMarkers.map >> FilesToMerge.txt
      export c
      c=idbv3CleanIndsMarkers
      sed -i.bak '/idbv3CleanIndsMarkers/d' FilesToMerge.txt
    fi
  fi
done

## Extract only SNP names for the purpose of extraction using plink
cut -d " " -f 2 idbv3CleanIndsMarkers.map > SnpsOnIdbv3BeforeMerge.txt

## Merge SNP chips and remove SNP with minor allele frequency less than 0.05
plink --file ${c} --merge-list FilesToMerge.txt --cow --extract SnpsOnIdbv3BeforeMerge.txt --maf 0.05 --missing --het --recode --out AllChipsFinal
## The line above was corrected so that no filtering for MAF is inculuded.
#plink --file ${c} --merge-list FilesToMerge.txt --cow --extract SnpsOnIdbv3BeforeMerge.txt --missing --het --recode --freqx --out AllChipsFinal
## plink --file LmHd --merge LmIdbv3.ped LmIdbv3.map --cow --extract SnpsOnHd.txt --missing --het --recode --out LmHdIdbv3

## Remove intermediate plink files
for i in ${geno}/*.ped; do
  a=`echo ${i} | sed -e 's/.*\///' -e 's/.ped//'`
  rm -f ${a}.* ${a}CleanInds.* ${a}CleanIndsMarkers.*
done

# Standard report
echo
pwd
date
