#!/bin/sh

#$ -N DrawManhattanPlotsForPotentiallyLethalHapXXBREEDXXcoreXXCORELENGTHXX
#$ -cwd
#$ -o DrawManhattanPlotsForPotentiallyLethalHapXXBREEDXXcoreXXCORELENGTHXX.out
#$ -e DrawManhattanPlotsForPotentiallyLethalHapXXBREEDXXcoreXXCORELENGTHXX.err
#$ -l h_rt=88:00:00
#$ -R y
#$ -pe sharedmem 1
#$ -l h_vmem=8G

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load igmm/apps/R/3.3.0

# Standard report

echo "Working directory:"
pwd
date

## This command was added since the file from the last job are not available immediately

./DrawManhattanPlotsForPotentiallyLethalHap.R > DrawManhattanPlotsForPotentiallyLethalHap.log 2>&1 &

# CPU and RAM tracking
JOB=$!
./cpumemlog.sh $JOB #-t=.1
#./cpumemlogplot.R cpumemlog_${JOB}.txt

# Standard report
echo
pwd
date
