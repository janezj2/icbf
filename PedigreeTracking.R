#!/usr/bin/env Rscript

## This script defines the origin of the haplotype

library(ggplot2)
library(reshape)

## This loop needs to be adjusted with the other script (look at previous 8_HaplypeStatistics.sh)
## setwd("../Analysis_Min50Perc_LmChAaSiHe/HaplotypeSummary/Min50PercAaInd/")

Base <- basename(getwd())

## Set core and breed working directory
BaseCore <- basename(getwd())
setwd("../")

BaseBreed <- basename(getwd())
setwd(BaseCore)

## Read in the data
BirthDates           <- read.csv("../../../../BirthDates/AnimalsID_birth_death_dates.csv", header=TRUE, stringsAsFactors=FALSE)
BirthDates$birthdate <- as.Date(BirthDates$birthdate,format = "%Y/%m/%d")
BirthDates$deathdate <- as.Date(BirthDates$deathdate,format = "%Y/%m/%d")
HaplotypeStat        <- read.table("FinalResults.txt", header=TRUE, stringsAsFactors=FALSE)
Pedig                <- read.table(paste("../../../Pedigree/", BaseBreed, "/PedigreeExtracted.txt", sep=""), header=FALSE, stringsAsFactors=FALSE, fill=TRUE)



# This bit was updated as constraints only for Sire-Dam or Sire-Mgs matings are met in the following script
## ## Extract potential lethal haplotypes - those with no homozygous progeny and haplotype frequency >2%
## LethalCandidatesFreq      <- HaplotypeStat[HaplotypeStat$NumberOfHomozygous==0 & HaplotypeStat$Perc>2, ]
## ## Extract potential lethal haplotypes - those with no homozygous progeny and probability of of observing no homozygous individual <0.05 (based on Sire - Dam mating)
## LethalCandidatesBernoulli <- HaplotypeStat[HaplotypeStat$ProbOfNoHomozVR2IdSireMgsChr<0.05 & !is.na(HaplotypeStat$ProbOfNoHomozVR2IdSireMgsChr) & HaplotypeStat$NumberOfHomozygous==0, ]
## ## Extract potential lethal haplotypes - those with no homozygous progeny and probability of of observing no homozygous individual <0.05 (based on Sire - Dam mating)
## LethalCandidatesBinomial  <- HaplotypeStat[!(is.na(HaplotypeStat$NumOfHomozygousProgenyMatingIdSireDamChr)) & HaplotypeStat$NumOfHomozygousProgenyMatingIdSireDamChr < HaplotypeStat$ExpectedNumOfHomoVR2CiLowIdSireDamChr & HaplotypeStat$NumOfHomozygousProgenyMatingIdSireMgsChr < HaplotypeStat$ExpectedNumOfHomoVR2CiLowIdSireMgsChr, ]
## ## Extract potential lethal haplotypes - those with p < 0.05 for Chi-Square test statostics
## LethalCandidatesChiSquare <- HaplotypeStat[HaplotypeStat$ChiSquareStatisticsSireDamPvalue < 0.05 & !is.na(HaplotypeStat$ChiSquareStatisticsSireDamPvalue) & HaplotypeStat$ChiSquareStatisticsSireMgsPvalue < 0.05 & !is.na(HaplotypeStat$ChiSquareStatisticsSireMgsPvalue) & HaplotypeStat$NumOfHomozygousProgenyMatingIdSireDamChr < HaplotypeStat$ExpectedNumOfHomoVR2IdSireDamChr, ]

## ## Paste partial tables into one
## LethalCandidates      <- rbind(LethalCandidatesFreq, LethalCandidatesBernoulli, LethalCandidatesBinomial, LethalCandidatesChiSquare)
## LethalCandidates      <- LethalCandidates[rowSums(is.na(LethalCandidates))!=ncol(LethalCandidates), ]



## Extract potential lethal haplotypes - those with no homozygous progeny and haplotype frequency >2%
LethalCandidatesFreq             <- HaplotypeStat[HaplotypeStat$NumberOfHomozygous==0 & HaplotypeStat$Perc>2, ]
## Extract potential lethal haplotypes - those with no homozygous progeny and probability of of observing no homozygous individual <0.05 (based on Sire - Dam mating)
LethalCandidatesBernoulliSireDam <- HaplotypeStat[HaplotypeStat$ProbOfNoHomozVR2IdSireDamChr<0.05 & !is.na(HaplotypeStat$ProbOfNoHomozVR2IdSireDamChr) & HaplotypeStat$NumOfHomozygousProgenyMatingIdSireDamChr==0, ]
## Extract potential lethal haplotypes - those with no homozygous progeny and probability of of observing no homozygous individual <0.05 (based on Sire - Mgs mating)
LethalCandidatesBernoulliSireMgs <- HaplotypeStat[HaplotypeStat$ProbOfNoHomozVR2IdSireMgsChr<0.05 & !is.na(HaplotypeStat$ProbOfNoHomozVR2IdSireMgsChr) & HaplotypeStat$NumOfHomozygousProgenyMatingIdSireMgsChr==0, ]
## Extract potential lethal haplotypes - those with lower number of observed homozygous individuals as expected from lower bound of bernouli distribution for Sire - Dam mating
LethalCandidatesBinomialSireDam  <- HaplotypeStat[!(is.na(HaplotypeStat$NumOfHomozygousProgenyMatingIdSireDamChr)) & HaplotypeStat$NumOfHomozygousProgenyMatingIdSireDamChr < HaplotypeStat$ExpectedNumOfHomoVR2CiLowIdSireDamChr, ]
## Extract potential lethal haplotypes - those with lower number of observed homozygous individuals as expected from lower bound of bernouli distribution for Sire - Mgs mating
LethalCandidatesBinomialSireMgs  <- HaplotypeStat[!(is.na(HaplotypeStat$NumOfHomozygousProgenyMatingIdSireMgsChr)) & HaplotypeStat$NumOfHomozygousProgenyMatingIdSireMgsChr < HaplotypeStat$ExpectedNumOfHomoVR2CiLowIdSireMgsChr, ]
## Extract potential lethal haplotypes - those with p < 0.05 for Chi-Square test statostics for Sire - Dam matings
LethalCandidatesChiSquareSireDam <- HaplotypeStat[HaplotypeStat$ChiSquareStatisticsSireDamPvalue < 0.05 & !is.na(HaplotypeStat$ChiSquareStatisticsSireDamPvalue) & HaplotypeStat$NumOfHomozygousProgenyMatingIdSireDamChr < HaplotypeStat$ExpectedNumOfHomoVR2IdSireDamChr, ]
## Extract potential lethal haplotypes - those with p < 0.05 for Chi-Square test statostics for Sire - Mgs matings
LethalCandidatesChiSquareSireMgs <- HaplotypeStat[HaplotypeStat$ChiSquareStatisticsSireMgsPvalue < 0.05 & !is.na(HaplotypeStat$ChiSquareStatisticsSireMgsPvalue) & HaplotypeStat$NumOfHomozygousProgenyMatingIdSireMgsChr < HaplotypeStat$ExpectedNumOfHomoVR2IdSireMgsChr, ]

## Paste partial tables into one
LethalCandidates      <- unique(rbind(LethalCandidatesFreq, LethalCandidatesBernoulliSireDam, LethalCandidatesBernoulliSireMgs, LethalCandidatesBinomialSireDam, LethalCandidatesBinomialSireMgs, LethalCandidatesChiSquareSireDam, LethalCandidatesChiSquareSireMgs))
LethalCandidates      <- LethalCandidates[rowSums(is.na(LethalCandidates))!=ncol(LethalCandidates), ]


for (i in 1:nrow(LethalCandidates)) {
  Chr              <- LethalCandidates[i, "Chromosome"]
  HaploLib         <- LethalCandidates[i, "HaploLib"]
  HaploLib         <- as.numeric(unlist(regmatches(HaploLib, gregexpr("[[:digit:]]+", HaploLib))))
  HaploID          <- LethalCandidates[i, "HaploID"]
  FinalHapIndCarry <- read.table(file=paste("../../../Chromosomes/", BaseBreed, "/Chromosome", Chr, "/Phasing/Phasing", BaseCore, "/PhasingResults/FinalHapIndCarry.txt", sep=""))
  Carriers         <- FinalHapIndCarry[, c(1, (HaploLib*2):(HaploLib*2+1))]
  Carriers[,2]     <- Carriers[,2] %in% HaploID
  Carriers[,3]     <- Carriers[,3] %in% HaploID
  Carriers$Sum     <- Carriers[,2] + Carriers[,3]

  if (nrow(Carriers[Carriers$Sum==2, ]) > 0) {
    print(paste("This haplotype occures as homozygous for ", nrow(Carriers[Carriers$Sum==2, ]), "individuals."))
  }
  Carriers                              <- merge(Carriers[Carriers$Sum>0, ], BirthDates, by.x="V1", by.y="ani_id")
  Carriers                              <- Carriers[order(Carriers$birthdate), ]

  ## For the oldest individual plot a pedigree following the blog post from Susan Johnson
  ## https://susanejohnston.wordpress.com/2014/10/10/step-by-step-a-stylised-pedigree-using-reshape-and-ggplot2/
  pedigree           <- merge(Pedig, BirthDates[,c("ani_id", "birthdate")], by.x="V1", by.y="ani_id")
  pedigree$BirthYear <- as.numeric(format(pedigree$birthdate,'%Y'))
  pedigree           <- pedigree[,c(-4)]
  names(pedigree)    <- c("ID", "FATHER", "MOTHER", "BirthYear")
  pedigree[pedigree==0] <- NA
  ped2 <- melt(pedigree, id.vars=c("ID"), measure.vars=c("MOTHER", "FATHER"))
  ped2 <- ped2[-which(is.na(ped2$value)),]
  names(ped2)[which(names(ped2) == "value")] <- "Parent.ID"
  ped2$Group <- 1:nrow(ped2)
  ped3 <- melt(ped2, id.vars = "Group", measure.vars=c("ID", "Parent.ID"))
  names(ped3)[3] <- "ID"
  ped3 <- merge(ped3, pedigree[,c("ID", "BirthYear")], by="ID")

  generateXcoord <- function(size) {

  if(size %% 2 != 0 & size != 1) {   # Check if size is odd
    newsize <- size - 1
    interval <- 1/newsize
    x <- seq(0, 1, interval)
  }

  if(size %% 2 == 0) {    # Check if size is even
    interval <- 1/size
    x <- seq(0, 1, interval)[-size-1] + diff(seq(0, 1, interval))/2
  }

  if(size == 1)
    x <- 0.5
    x
  }

  #~~ Create an object to save the x coordinates within
  xcoords <- NULL

  for(j in unique(ped3$BirthYear)){

    # Extract the number of Unique IDs per year and generate X coords
    ids  <- unique(ped3$ID[which(ped3$BirthYear == j)])
    newx <- generateXcoord(length(ids)) # generate X coordinates

    # Append to xcoords
    xcoords <- rbind(xcoords,
                     data.frame(ID = ids,
                                x = sample(newx, size = length(newx), replace = F)))

    rm(ids, newx)
  }

  # Merge with ped3
  ped3 <- merge(ped3, xcoords)

  ## Extract only carrier individuals and keep only connections between carrier individuals
  ped4 <- ped3[ped3$ID %in% Carriers$V1, ]

  birthyears <- sort(unique(ped3$BirthYear[!is.na(ped3$BirthYear)]))

  ## Calculate number of carrier progeny for carrier parents
  CarriersPedig  <- Pedig[Pedig$V1 %in% Carriers$V1, ]
  CarriersPedig[!(CarriersPedig$V2 %in% Carriers$V1), "V2"] <- 0
  CarriersPedig[!(CarriersPedig$V3 %in% Carriers$V1), "V3"] <- 0
  CarrierProgeny <- data.frame(table(c(CarriersPedig$V2, CarriersPedig$V3)))
  CarrierProgeny <- merge(CarrierProgeny, Carriers[,c("V1","ITT")], by.x="Var1", by.y="V1")
  CarrierProgeny <- merge(CarrierProgeny, unique(ped4[,c("ID","x","BirthYear")]), by.x="Var1", by.y="ID")
  try(CarrierProgeny[CarrierProgeny$x < 0.20, "x"] <- 0.20, silent=TRUE)
  try(CarrierProgeny[CarrierProgeny$x > 0.80, "x"] <- 0.80, silent=TRUE)
  ## Extract only those with more than 50 carrier progeny
  CarrierProgeny <- CarrierProgeny[CarrierProgeny$Freq > 50 & CarrierProgeny$Var1 !=0, ]

  ## Plot Complete pedigree
  pdf(file=paste("PedigrePlotComplete", Base, "Chr", Chr, "HaploLib", HaploLib, "HaploID", HaploID, ".pdf", sep=""), width=13/2.54,height=20/2.54, pointsize=12)
  print(ggplot(ped3, aes(x, -BirthYear)) +
  geom_line(aes(group = Group), alpha = 0.1) +
  geom_point(size = 0.5) +
  geom_point(size = 0.1) +
  theme(legend.position  = "none",
        axis.text.x      = element_blank(),
        axis.text.y      = element_text(colour = "darkgrey"),
        axis.ticks.y     = element_blank(),
        axis.ticks.x     = element_blank(),
        panel.grid       = element_blank(),
        plot.background  = element_rect(),
        panel.background = element_blank()) +
  theme(plot.margin = unit(c(1.5, 1.5, 1.5, 1.5), units = "cm")) +
  scale_y_continuous(breaks = -seq(min(birthyears), max(birthyears), 1),
                     labels =  seq(min(birthyears), max(birthyears), 1)) +
  labs(x = "", y = "Birth year") +
  ggtitle(paste(Base, "Chr", Chr, "HaploLib", HaploLib, "HaploID", HaploID, sep="")))
  dev.off()

  ## Plot Complete pedigree and carriers
  pdf(file=paste("PedigrePlotCompleteCarriers", Base, "Chr", Chr, "HaploLib", HaploLib, "HaploID", HaploID, ".pdf", sep=""), width=13/2.54,height=20/2.54, pointsize=12)
  print(ggplot(ped3, aes(x, -BirthYear)) +
  geom_line(aes(group = Group), alpha = 0.1) +
  geom_point(size = 0.1) +
  theme(plot.title       = element_text(hjust = 0.5),
        legend.position  = "none",
        axis.text.x      = element_blank(),
        axis.text.y      = element_text(colour = "darkgrey"),
        axis.ticks.y     = element_blank(),
        axis.ticks.x     = element_blank(),
        panel.grid       = element_blank(),
        plot.background  = element_rect(),
        panel.background = element_blank()) +
  theme(plot.margin = unit(c(1.5, 1.5, 1.5, 1.5), units = "cm")) +
  scale_y_continuous(breaks = -seq(min(birthyears), max(birthyears), 1),
                     labels =  seq(min(birthyears), max(birthyears), 1)) +
  labs(x = "", y = "Birth year") +
  ggtitle(paste(Base, "Chr", Chr, "HaploLib", HaploLib, "HaploID", HaploID, sep="")) +
  geom_point(data = ped4, color='red', size = 0.5) +
  geom_line(data = ped4, aes(group = Group), color='red', alpha = 0.1) +
  geom_point(data = ped4, size = 0.1))
  dev.off()

  ## Plot pedigree for carriers only
  pdf(file=paste("PedigrePlotCarriers", Base, "Chr", Chr, "HaploLib", HaploLib, "HaploID", HaploID, ".pdf", sep=""), width=13/2.54,height=20/2.54, pointsize=12)
  print(ggplot(ped4, aes(x, -BirthYear)) +
  geom_line(aes(group = Group), color='red', alpha = 0.1) +
  geom_point(size = 0.5, color='red') +
  geom_point(size = 0.1) +
  theme(legend.position  = "none",
        axis.text.x      = element_blank(),
        axis.text.y      = element_text(colour = "darkgrey"),
        axis.ticks.y     = element_blank(),
        axis.ticks.x     = element_blank(),
        panel.grid       = element_blank(),
        plot.background  = element_rect(),
        panel.background = element_blank()) +
  theme(plot.margin = unit(c(1.5, 1.5, 1.5, 1.5), units = "cm")) +
  scale_y_continuous(breaks = -seq(min(birthyears), max(birthyears), 1),
                     labels =  seq(min(birthyears), max(birthyears), 1)) +
  labs(x = "", y = "Birth year") +
  ggtitle(paste(Base, "Chr", Chr, "HaploLib", HaploLib, "HaploID", HaploID, sep="")) +
  annotate("text", x=CarrierProgeny$x, y=-CarrierProgeny$BirthYear+0.5, label=paste(CarrierProgeny$ITT, " (", CarrierProgeny$Freq, ")", sep=""), size=2.5) +
  geom_line(aes(group = Group), color='red', alpha = 0.1))
  dev.off()
}
