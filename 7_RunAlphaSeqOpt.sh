#!/bin/bash

## Create working directory for AlphaSeqOpt

export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export prog=~/Hpc/Illumina/Janez/Programs
export work=~/Hpc/Illumina/Janez
export fCHR="1" #From chromosome
export tCHR="29" #To chromosome
export HapExcl="1 2 3 4 5 6 11 0.01Perc 0.05Perc 0.5Perc 5Perc"
export Perc="0 0 0 0 0 0 0 1 1 1 1" #This is done because for scenarios with percentage treshold we substitute treshold restriction as s/XXHAPLOTHRESXX/0/g
export seq="0" #This is parameter is only created to select appropriate element from array. It is updated within a loop

## Load module (this is required for AlphaSeqOpt)
module load intel/2016

cd ${A} || exit

if [[ ! -d AlphaSeqOpt ]]; then
  mkdir AlphaSeqOpt || exit
fi

cd AlphaSeqOpt || exit

for i in ${A}/SelectedInd/*; do

  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`

  if [ ! -d ${a} ]; then
    mkdir ${a}
  fi

  cd ${a} || exit

  for j in ${HapExcl}; do
    seq=$(($seq+1))

    if [[ ! -d ${j} ]]; then
      mkdir ${j}
    fi

    cd ${j} || exit

    ln -sf ${A}/Pedigree/${a}/AlphaRecodedPedigree.txt Pedigree.txt
    ln -sf ${work}/PreSequenceInfo/IndAlreadySequenced.txt .
    ln -sf ${prog}/AlphaSuite/AlphaSeqOpt .

    b=`wc -l Pedigree.txt | cut -d" " -f1`

    c=`echo ${Perc} | cut -d" " -f$seq`

    cat ${prog}/AlphaSuite/AlphaSeqOptSpec_Seqence.txt | sed -e "s/XXNROFINDXX/${b}/g" \
                                                     -e "s/XXTCHRXX/${tCHR}/g" > AlphaSeqOptSpec_Seqence.txt

    if [[ ${c} -eq 0 ]]; then
      sed -i "s/XXHAPLOTHRESXX/${j}/g" AlphaSeqOptSpec_Seqence.txt
    fi

    if [[ ${c} -eq 1 ]]; then
      sed -i "s/XXHAPLOTHRESXX/0/g" AlphaSeqOptSpec_Seqence.txt
    fi

#    if [[ ! -d Phase ]]; then
#      mkdir Phase
#    fi
#
#    cd Phase || exit
#
#    for k in `seq 1 1 ${tCHR}`; do
#
#      ln -sf ${A}/Chromosomes/${a}/Chromosome${k}/Phasing/PhasingResults/FinalPhase.txt Chromosome${k}.txt
#
#      if [ -e ${A}/Chromosomes/${a}/Chromosome${k}/ch_coded ]; then
#        d=`head -n 1 ${A}/Chromosomes/${a}/Chromosome${k}/ch_coded | cut -d " " -f 2- | awk '{print NF}'`
#      else
#        d=`head -n 1 ${A}/Chromosomes/${a}/Chromosome${k}/Results/ImputeGenotypesHMM.txt | cut -d " " -f 2- | awk '{print NF}'`
#      fi
#
#      sed -i "s/XXNROFSNPCHR${k}XX/${d}/" ../AlphaSeqOptSpec_Seqence.txt
#
#    done
#
#    cd ..


    if [[ ! -d Cores ]]; then
      mkdir Cores
    fi

    cd Cores || exit

    for k in `seq ${fCHR} 1 ${tCHR}`; do

      if [[ ${c} -eq 0 ]]; then
        ln -sf ${A}/Chromosomes/${a}/Chromosome${k}/Phasing/PhasingResults/FinalHapIndCarry.txt Chromosome${k}.txt
      fi

      d=`head -1 Chromosome${k}.txt | awk '{print NF}'`
      d=$(((${d}-1)/2))

      sed -i "s/XXNROFCORESCHR${k}XX/${d}/" ../AlphaSeqOptSpec_Seqence.txt

    done

    cd ..

    ln -sf AlphaSeqOptSpec_Seqence.txt AlphaSeqOptSpec.txt

    cat ${work}/Scripts/qsub_AlphaSeqOpt.sh | sed -e "s/XXBREEDXX/${a}/g" \
                                                  -e "s/XXHAPLOTHRESXX/${j}/g" > qsub_AlphaSeqOpt.sh

    rm -f ASeqOpt.err ASeqOpt.out

    qsub -hold_jid P8AP${a}Chr1,P8AP${a}Chr2,P8AP${a}Chr3,P8AP${a}Chr4,P8AP${a}Chr5,P8AP${a}Chr6,P8AP${a}Chr7,P8AP${a}Chr8,P8AP${a}Chr9,P8AP${a}Chr10,P8AP${a}Chr11,P8AP${a}Chr12,P8AP${a}Chr13,P8AP${a}Chr14,P8AP${a}Chr15,P8AP${a}Chr16,P8AP${a}Chr17,P8AP${a}Chr18,P8AP${a}Chr19,P8AP${a}Chr20,P8AP${a}Chr21,P8AP${a}Chr22,P8AP${a}Chr23,P8AP${a}Chr24,P8AP${a}Chr25,P8AP${a}Chr26,P8AP${a}Chr27,P8AP${a}Chr28,P8AP${a}Chr29 qsub_AlphaSeqOpt.sh

    cd ..

  done

  seq="0"

  cd ..

done
