#!/bin/bash

export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts

cd ${A}/HaplotypeSummary || exit

for i in ${A}/SelectedInd/*; do

  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`

  cd ${a} || exit

  rm -f FunctionalAnnotation.out FunctionalAnnotation.err
  ln -sf ${S}/FunctionalAnnotation.R
  ln -sf ${S}/LethalHaplotypeFormatChange.R
  cp -rf ${S}/qsub_13_FunctionalAnnotation.sh .
  sed -i -e "s/XXBREEDXX/${a}/g" qsub_13_FunctionalAnnotation.sh
  qsub -hold_jid PedigTracking${a} qsub_13_FunctionalAnnotation.sh

  cd ..

done
