#!/bin/sh

cd ../Pedigree/

cat << EOF > PreparePedigree.R
#!/usr/bin/env Rscript
library("pedigree")
library("gdata")

########################################################################################################################################
# This bit was commented as now we have pedigree build only on the genotyped individuals
# ## Import the data
# Ped         <- read.table(file="PedigreeOrig.txt", header=TRUE, stringsAsFactors = FALSE)
# 
# ## As there are some individuals duplicated (The reason for this is that Matt send extra data wher some individuals are duplicated)
# ## I will use only the one that were sent last time
# 
# tmp <- data.frame(table(Ped\$techid))
# tmp <- tmp[tmp\$Freq==2,]
# 
# PedOld   <- Ped[1:38931751,]
# PedExtra <- Ped[38931752:nrow(Ped),]
# PedOld   <- PedOld[!(PedOld\$techid %in% tmp\$Var1), ]
# Ped      <- rbind(PedOld,PedExtra)
# write.table(Ped, file="Pedigree.txt", row.names=FALSE, col.names=TRUE, quote=FALSE, sep="\t")
########################################################################################################################################

########################################################################################################################################
# This bit has repaced the first part due to the changes in pedigree file on 02.08.2017
Ped         <- read.table(file="PedigreeOrig.txt", header=TRUE, stringsAsFactors = FALSE, fill = TRUE)

## If sire and dam are not inclued in the techid column add them in
Sires <- data.frame(unique(Ped\$sire_tech_no[!(Ped\$sire_tech_no %in% Ped\$techid)]))
Dams  <- data.frame(unique(Ped\$dam_tech_no[!(Ped\$dam_tech_no %in% Ped\$techid)]))
names(Sires) <- names(Dams) <- "techid"
Sires\$sire_tech_no <- Dams\$sire_tech_no <- Sires\$dam_tech_no <- Dams\$dam_tech_no <- 0
Sires\$sex <- "M"
Dams\$sex  <- "F"
Ped       <- rbind(Ped,Sires,Dams)
Ped       <- Ped[!(Ped\$techid == 0) & !(is.na(Ped\$techid)), ]
write.table(Ped, file="Pedigree.txt", row.names=FALSE, col.names=TRUE, quote=FALSE, sep="\t")
########################################################################################################################################

Ped         <- Ped[,-4]
names(Ped)  <- c("id","sire","dam")

## Order pedigree so that parents are always before progeny
ord             <- orderPed(Ped)
Ped             <- Ped[order(ord),]
Ped[is.na(Ped)] <- 0
write.table(Ped, file="PedReordered.txt", row.names=FALSE, col.names=FALSE, quote=FALSE)

Ped\$Seq        <- 1:nrow(Ped)
Ped             <- merge(Ped,Ped[,c("id","Seq")], by.x = "sire", by.y = "id", all.x = TRUE)
Ped             <- Ped[,c("id","sire","dam","Seq.x","Seq.y")]
names(Ped)      <- c("id","sire","dam","idSeq","sireSeq")
Ped             <- merge(Ped,Ped[,c("id","idSeq")], by.x = "dam", by.y = "id", all.x = TRUE)
Ped             <- Ped[,c("id","sire","dam","idSeq.x","sireSeq","idSeq.y")]
names(Ped)      <- c("id","sire","dam","idSeq","sireSeq","damSeq")
Ped             <- Ped[order(Ped\$idSeq),]
Ped[is.na(Ped)] <- 0

write.fwf(Ped[,c("idSeq","sireSeq","damSeq","id")], file="PedigreeRecoded.txt", width=c(15,15,15,15), colnames=FALSE, rownames=FALSE, quote = FALSE)
EOF

chmod +700 PreparePedigree.R

cat << EOF > qsub_PreparePedigree.sh
#!/bin/sh

#$ -N PreparePedigree
#$ -cwd
#$ -o PreparePedigree.out
#$ -e PreparePedigree.err
#$ -l h_rt=6:00:00
#$ -R y
#$ -pe sharedmem 1
#$ -l h_vmem=16G

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load igmm/apps/R/3.3.0

# Standard report

echo "Working directory:"
pwd
date

./PreparePedigree.R
rm PreparePedigree.R
rm PreparePedigree.err
rm PreparePedigree.out
rm qsub_PreparePedigree.sh

# Standard report
echo
pwd
date
EOF

qsub qsub_PreparePedigree.sh
